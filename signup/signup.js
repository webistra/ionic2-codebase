var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, MenuController, LoadingController, Events } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { MyqueuePage } from '../myqueue/myqueue';
import { LoginPage } from '../login/login';
import { FormBuilder, Validators } from '@angular/forms';
import { CommondataProvider } from '../../providers/commondata/commondata';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Storage } from '@ionic/storage';
//let apiUrl = 'http://172.16.0.101/PROJECTS/Firebase/public/api/';
var apiUrl = 'http://ec2-52-221-54-107.ap-southeast-1.compute.amazonaws.com/PROJECTS/Firebase/website/public/api/';
var SignupPage = (function () {
    function SignupPage(navCtrl, menuCtrl, formBuilder, http, loadingCtrl, commondata, events, af, db, storage) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.formBuilder = formBuilder;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.commondata = commondata;
        this.events = events;
        this.af = af;
        this.db = db;
        this.storage = storage;
        this.devicetype = '';
        this.signuperror = false;
        this.errmessage = '';
        this.signupForm = formBuilder.group({
            email: ['', Validators.compose([Validators.maxLength(40), Validators.pattern(/^([\w-\.]+@(?!lsu.edu)([\w-]+\.)+[\w-]{2,4})?$/), Validators.required])],
            password: ['', Validators.compose([Validators.minLength(8), Validators.maxLength(16), Validators.required])],
            cpassword: ['', Validators.compose([Validators.minLength(8), Validators.maxLength(16), Validators.required])],
        }, { validator: this.checkIfMatchingPasswords('password', 'cpassword') });
    }
    SignupPage.prototype.checkIfMatchingPasswords = function (passwordKey, passwordConfirmationKey) {
        return function (group) {
            var passwordInput = group.controls[passwordKey], passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value) {
                return passwordConfirmationInput.setErrors({ notEquivalent: true });
            }
            else {
                return passwordConfirmationInput.setErrors(null);
            }
        };
    };
    SignupPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    SignupPage.prototype.backbutton = function () {
        this.navCtrl.push(LoginPage);
    };
    SignupPage.prototype.home = function () {
        var _this = this;
        this.signuperror = false;
        this.errmessage = '';
        if (navigator.platform == 'Android') {
            this.devicetype = '0';
        }
        else if (navigator.platform == 'iPhone') {
            this.devicetype = '1';
        }
        else {
            this.devicetype = '';
        }
        if (!this.signupForm.valid) {
        }
        else {
            return new Promise(function (resolve, reject) {
                var loader = _this.loadingCtrl.create({
                    content: 'Please wait...',
                    dismissOnPageChange: true
                });
                loader.present();
                _this.validEmailCheck = _this.email;
                _this.validPasswordCheck = _this.password;
                var headers = new Headers();
                var body = JSON.stringify({
                    email: _this.email,
                    password: _this.password,
                    deviceType: _this.devicetype
                });
                headers.append('Content-Type', 'application/json');
                _this.http.post(apiUrl + 'signup', body, { headers: headers })
                    .subscribe(function (res) {
                    loader.dismiss();
                    resolve(res.json());
                    _this.data = res.json();
                    _this.signuperror = false;
                    localStorage.setItem('chatfirsttype', _this.data.data.userType);
                    _this.commondata.userid = _this.data.data.userId;
                    _this.commondata.usertoken = _this.data.data.token;
                    _this.commondata.useremail = _this.data.data.email;
                    _this.commondata.usertype = _this.data.data.userType;
                    _this.commondata.username = _this.data.data.name;
                    localStorage.setItem('chatfirstName', _this.data.data.name);
                    localStorage.setItem('chatfirsttoken', _this.data.data.token);
                    localStorage.setItem('chatfirstid', _this.data.data.userId);
                    localStorage.setItem('chatfirstEmail', _this.data.data.email);
                    localStorage.setItem('emailrequirecheck', "");
                    localStorage.setItem('imagetosend', "");
                    _this.events.publish('user:login');
                    _this.af.auth.createUserWithEmailAndPassword(_this.email, _this.password).then(function (data) {
                        _this.storage.set('uid', data.uid);
                        var currentUserRef = _this.db.object("/users/" + data.uid);
                        localStorage.setItem('companyid', data.uid);
                        // alert(data.uid)
                        currentUserRef.set({ email: _this.email });
                    }, function (error) {
                    });
                    _this.navCtrl.push(MyqueuePage);
                    console.log(_this.data);
                }, function (err) {
                    loader.dismiss();
                    _this.signuperror = true;
                    if (err.status == '400') {
                        _this.errmessage = 'Password must be atleast 8 characters.';
                    }
                    if (err.status == '401') {
                        _this.errmessage = 'Email Id already taken.Use a different one.';
                    }
                });
            });
        }
    };
    SignupPage.prototype.signin = function () {
        this.navCtrl.push(LoginPage);
    };
    return SignupPage;
}());
SignupPage = __decorate([
    Component({
        selector: 'page-signup',
        templateUrl: 'signup.html'
    }),
    __metadata("design:paramtypes", [NavController, MenuController, FormBuilder, Http, LoadingController, CommondataProvider, Events, AngularFireAuth, AngularFireDatabase, Storage])
], SignupPage);
export { SignupPage };
//# sourceMappingURL=signup.js.map