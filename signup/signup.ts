import { Component } from '@angular/core';
import { Platform, NavController, MenuController, LoadingController, Events } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { MyqueuePage } from '../myqueue/myqueue';
import { LoginPage } from '../login/login';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommondataProvider } from '../../providers/commondata/commondata';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Storage } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm';
import {MessagingServiceProvider} from '../../providers/messaging-service/messaging-service'

// API base url
let apiUrl = 'https://stag3.mobiloitte.com/PROJECTS/Firebase/website/public/api/';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
    /** define all variables **/
    signupForm : FormGroup;
    public email: string;
    public password: string;
    devicetype = '';
    signuperror = false;
    errmessage = '';
    validEmailCheck: any;
    validPasswordCheck: any;
    data: any;

    constructor(public msgService: MessagingServiceProvider, public platform: Platform, public navCtrl: NavController, public menuCtrl: MenuController, public formBuilder: FormBuilder, public http: Http, public loadingCtrl: LoadingController, public commondata: CommondataProvider, public events: Events, public af: AngularFireAuth, public db: AngularFireDatabase, public storage: Storage, public fcm:FCM) {
      
      /** sign up form validaton pattern **/
      this.signupForm = formBuilder.group({
      email: ['', Validators.compose([Validators.maxLength(40), Validators.pattern(/^([\w-\.]+@(?!lsu.edu)([\w-]+\.)+[\w-]{2,4})?$/), Validators.required])],
      password: ['', Validators.compose([Validators.minLength(8), Validators.maxLength(16), Validators.required])],
      cpassword: ['', Validators.compose([Validators.minLength(8), Validators.maxLength(16), Validators.required])],
      },{validator: this.checkIfMatchingPasswords('password', 'cpassword')});

      if(this.platform.is('ios') || this.platform.is('android')){
        fcm.getToken().then(token=>{
          if(token!=null)
            localStorage.setItem('FCMToken',token);
          else
            this.checkToken();
        })
      }else{
        //this.msgService.getPermission();
      }
    }
    /** check password and confirm password **/
    checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
      return (group: FormGroup) => {
        let passwordInput = group.controls[passwordKey],
            passwordConfirmationInput = group.controls[passwordConfirmationKey];
        if (passwordInput.value !== passwordConfirmationInput.value) {
          return passwordConfirmationInput.setErrors({notEquivalent: true})
        }
        else {
            return passwordConfirmationInput.setErrors(null);
        }
      }
    }

    ionViewWillEnter(){
        this.menuCtrl.enable(false);
    }
    /** functionality of back button **/
    backbutton(){
      this.navCtrl.push(LoginPage)
    }
    /** get FCM token and set to localStorage variable **/
    checkToken(){
      this.fcm.getToken().then(token=>{
        if(token!=null)
          localStorage.setItem('FCMToken',token);
        else
          this.checkToken();
      })
    }
    /** click on Lets start button to sign up user  **/
    home(){
      this.signuperror = false;
      this.errmessage = '';
      if(navigator.platform == 'Android'){
          this.devicetype = '0'
      }else if(navigator.platform == 'iPhone'){
          this.devicetype = '1'
      }else{
          this.devicetype = ''
      }if(!this.signupForm.valid){
      } else {
        return new Promise((resolve, reject) => {
          let loader = this.loadingCtrl.create({
          content: 'Please wait...',
          dismissOnPageChange: true
        })
        loader.present();
        this.validEmailCheck = this.email;
        this.validPasswordCheck = this.password;
        let headers = new Headers();
        let body = JSON.stringify({
            email: this.email,
            password: this.password,
            deviceType: this.devicetype
            });
        headers.append('Content-Type', 'application/json');
        /** Sign up in backend database **/
        this.http.post(apiUrl+'signup', body, {headers: headers})          
          .subscribe(res => {
            loader.dismiss();
            this.data = res.json()
            this.signuperror = false;
            /** store user information in local Storage **/
            localStorage.setItem('chatfirsttype',this.data.data.userType)
            /** store info in service variables **/
            this.commondata.userid = this.data.data.userId;
            this.commondata.usertoken = this.data.data.token;
            this.commondata.useremail = this.data.data.email;
            this.commondata.usertype = this.data.data.userType;
            this.commondata.username = this.data.data.name;
            /** store user information in local Storage **/
            localStorage.setItem('chatfirstName',this.data.data.name)
            localStorage.setItem('chatfirsttoken',this.data.data.token)
            localStorage.setItem('chatfirstid',this.data.data.userId)
            localStorage.setItem('chatfirstEmail',this.data.data.email)
            localStorage.setItem('emailrequirecheck',"")
            localStorage.setItem('imagetosend',"")
            /** firebase authentication method sign up using email id and password **/
            this.af.auth.createUserWithEmailAndPassword(this.email,this.password).then((data) => {
              this.storage.set('uid', data.uid);
              localStorage.setItem('firebaseuid',data.uid);
              let currentUserRef = this.db.object(`/users/${data.uid}`);
              localStorage.setItem('companyid',data.uid)
              currentUserRef.set({'email': this.email,'FCMToken':localStorage.getItem('FCMToken'),'id':localStorage.getItem('chatfirstid'),'userType':localStorage.chatfirsttype,isOnline:true});
              resolve(res.json());
            }, (error) => {
            });                          
          }, (err) => {
            loader.dismiss();
            this.signuperror = true;
            if(err.status == '400'){
              this.errmessage = 'Password must be atleast 8 characters.'
            }
            if(err.status == '401'){
              this.errmessage = 'Email Id already taken.Use a different one.'
            }
          });
        }).then(()=>{
          this.events.publish('user:login');
          this.navCtrl.push(MyqueuePage);
        });
      }
    }
    /** when user click on log in **/
    signin(){
        this.navCtrl.push(LoginPage);
    }

}
