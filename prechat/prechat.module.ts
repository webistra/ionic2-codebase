import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrechatPage } from './prechat';

@NgModule({
  declarations: [
    PrechatPage,
  ],
  imports: [
    IonicPageModule.forChild(PrechatPage),
  ],
  exports: [
    PrechatPage
  ]
})
export class PrechatPageModule {}
